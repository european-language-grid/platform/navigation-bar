# Navigation bar source of truth for the ELG websites

## Description

Implementation in pure HTML CSS of the ELG navigation bar.
The navigation bar works for both the desktop and the mobile view.
It includes the new items coming from the Wordpress websites in addition to the main items already in place for the live grid.

## Implementation

All the HTML files are simply here to simulate pages and the navigation bar in all these pages is the same.

The `assets` folder contains the icons, the logos, and CSS custom properties (variables). The icons and the CSS custom properties are also used in the Angular frontend and could also be used in the React frontend to help synchronizing the frontends using CSS custom properties.

The `nav.css` and the `footer.css` files contain respectively the css used for the navigation bar and the footer.

Both the navigation and the footer do not use javascript which should facilitate the integration in Angular and React.
The `active` and `child-active` classes simply need to be added to the elements depending on the page.

The code may not be optimzed and contributions/remarks.feedback are welcomed 🙂

## Run

### Using `npx serve`

Clone the repository and open a terminal in the root of the project. Then run:

```bash
npx serve
```

The navigation bar should be accessible at http://localhost:3000.

### Using Docker

Simply run:

```bash
docker run -p 3000:3000 registry.gitlab.com/european-language-grid/platform/navigation-bar:latest
```

The navigation bar should be accessible at http://localhost:3000 after a couple of seconds.

## Test

You can test the navigation bar by clicking the various items, however, most of them are linking to `/`. Only `Catalogue`, `Documentation & Publications`, `About`, `ELG EU Project`, and `Open Calls for Pilot Projects` are working.

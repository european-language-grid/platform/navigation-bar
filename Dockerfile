FROM    mhart/alpine-node
EXPOSE  3000

ADD     ./    /site
WORKDIR /site

CMD ["npx", "serve"]